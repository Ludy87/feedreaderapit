<?php

    function createDate($oldDate) {
        return DateTime::createFromFormat('D, d M Y H:i:s', substr($oldDate, 0, strlen($oldDate)-4))->format('d.m.Y H:i:s');
    }
	
	function send($title, $creator, $link) {
		$post_content = array('to' => TOPIC_ADRESS, 'data' => array('title' => $title, "creator" => $creator, "link" => $link));
		$httpheader = array('Content-Type:application/json', 'Authorization:key='.SERVER_KEY);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, FIREBASE_SERVER_FCM);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $httpheader);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_content));
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
	
	function url_get_contents ($url) {
		if (!function_exists('curl_init')){ 
			die('CURL is not installed!');
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output = curl_exec($ch);
		curl_close($ch);
		return $output;
	}

	function put($key, $value) {
		$firebase_server = FIREBASE_SERVER_DATABASE."feeds.json";
		$httpheader = array('Content-Type:application/json');
		$post_content = array($key => $value);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $firebase_server);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $httpheader);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_content));
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}
?>