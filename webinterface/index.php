<?php 
    define(SERVER_KEY, "");                 // euer Serverschlüssel
    define(TOPIC_ADRESS, "/topics/info");   // identifiziert unsere Nachricht als unsere und sendet an alle Geräte die registriert sind
    define(FIREBASE_SERVER_FCM, "https://fcm.googleapis.com/fcm/send");
	define(FIREBASE_SERVER_DATABASE, "https://{Project ID}.firebaseio.com/");
    header("Content-type: application/json; charset=utf-8");

    $result_rss    = requireToVar('./rss.php');               // hier wird unsere rss.php eingelesen um den generierten JsonString zu verwenden
    $items         = json_decode($result_rss, true)['item'];  // filtert nur die Item's

    foreach($items as $item) {
        if (substr($item['pubDate'], 0, strlen($item['pubDate'])-9) == date('d.m.Y')) {
			$file_get = url_get_contents(FIREBASE_SERVER_DATABASE."feeds/" . md5($item['title'].$item['pubDate']) . ".json");

			if ($file_get == "null") {
				echo send($item['title'], $item['creator'], $item['link']);
				put(md5($item['title'].$item['pubDate']), $item['pubDate']);
			}
		}
    }

    //
    // einlesen von Server eigenen Dateien
    //
    function requireToVar($file){
        ob_start();      // Ausgabepufferung aktivieren, verhindert das unsere rss.php gleich ausführt wird
        require($file);
        return ob_get_clean();
    }
?>