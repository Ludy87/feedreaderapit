<?php
	require_once('./utils.php');
    header("Content-type: application/json; charset=utf-8");

    $rss_url = 'https://www.androidpit.de/feed/main.xml';
    $feed = new DOMDocument();
    $feed->load($rss_url);
    $slashNS =             "http://purl.org/dc/elements/1.1/";
    $parentElement =       $feed->getElementsByTagName('channel')->item(0);
    $json = array();
    $json['title'] =       $parentElement->getElementsByTagName('title')->item(0)->firstChild->nodeValue;
    $json['link'] =        $parentElement->getElementsByTagName('link')->item(0)->firstChild->nodeValue;
    $json['description'] = $parentElement->getElementsByTagName('description')->item(0)->firstChild->nodeValue;
    $json['language'] =    $parentElement->getElementsByTagName('language')->item(0)->firstChild->nodeValue;
    $json['pubDate'] =     createDate($parentElement->getElementsByTagName('pubDate')->item(0)->firstChild->nodeValue);
    $json['creator'] =     $parentElement->getElementsByTagNameNS($slashNS, 'creator')->item(0)->firstChild->nodeValue;
    $items =               $parentElement->getElementsByTagName('item');
    $json['item'] = array();
    foreach ($items as $item) {
        $title =          $item->getElementsByTagName('title')->item(0)->firstChild->nodeValue;
        $link =           $item->getElementsByTagName('link')->item(0)->firstChild->nodeValue;
        $description =    $item->getElementsByTagName('description')->item(0)->firstChild->nodeValue;
        $pubDate =        createDate($item->getElementsByTagName('pubDate')->item(0)->firstChild->nodeValue);
        $creator =        $item->getElementsByTagNameNS($slashNS, 'creator')->item(0)->firstChild->nodeValue;
        $json['item'][] = array("title"=>$title,"link"=>$link,"description"=>$description,"pubDate"=>$pubDate,"creator"=>$creator);
    }
    echo json_encode($json);
?>