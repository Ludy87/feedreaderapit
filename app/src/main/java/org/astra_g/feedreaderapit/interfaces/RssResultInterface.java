package org.astra_g.feedreaderapit.interfaces;

public interface RssResultInterface {
    void onResult(String result);

    void onResponseCode(int responseCode);
}