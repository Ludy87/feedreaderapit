package org.astra_g.feedreaderapit;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.gson.Gson;

import org.astra_g.feedreaderapit.adapter.RssFeedAdapter;
import org.astra_g.feedreaderapit.asynctaskes.LoadRss;
import org.astra_g.feedreaderapit.gsons.Item;
import org.astra_g.feedreaderapit.gsons.Parent;
import org.astra_g.feedreaderapit.interfaces.RssResult;
import org.astra_g.feedreaderapit.utilities.ItemClickSupport;

import java.util.ArrayList;
import java.util.Collections;

public class FeedActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private static final String RSS_URL = "http://192.168.0.22/firebase/rss.php";

    private RssFeedAdapter rssFeedAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        new LoadRss(rssInterface).execute(RSS_URL);
    }

    private RssResult rssInterface = new RssResult() {
        @Override
        public void onResult(String result) {
            if (result != null) {
                Gson parentGson = new Gson();

                Parent parent               = parentGson.fromJson(result, Parent.class);
                String parentTitle          = parent.getParentTitle();
                String parentLink           = parent.getParentLink();
                String parentDescription    = parent.getParentDescription();
                String parentLanguage       = parent.getParentLanguage();
                String parentPubDate        = parent.getParentPubDate();
                String parentCreator        = parent.getParentCreator();

                Item[] items = parent.getParentItems();
                final ArrayList<Item> list = new ArrayList<>();
                Collections.addAll(list, items);
                rssFeedAdapter = new RssFeedAdapter(list);
                mRecyclerView.setAdapter(rssFeedAdapter);
                ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(list.get(position).getLink()));
                        startActivity(intent);
                    }
                });
            }
        }
    };
}