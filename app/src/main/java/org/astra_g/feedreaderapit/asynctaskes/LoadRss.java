package org.astra_g.feedreaderapit.asynctaskes;

import android.os.AsyncTask;
import android.util.Log;

import org.astra_g.feedreaderapit.interfaces.RssResult;
import org.astra_g.feedreaderapit.utilities.StreamToString;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class LoadRss extends AsyncTask<String, String, String> {

    private RssResult rssResultInterface;
    private static final String TAG = LoadRss.class.getSimpleName();

    public LoadRss(RssResult rssInterface) {
        this.rssResultInterface = rssInterface;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            URL url = new URL(params[0]);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            rssResultInterface.onResponseCode(connection.getResponseCode());
            return StreamToString.streamToString(connection.getInputStream());
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        rssResultInterface.onResult(s);
    }
}