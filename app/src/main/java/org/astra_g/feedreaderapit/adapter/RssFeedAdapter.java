package org.astra_g.feedreaderapit.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.astra_g.feedreaderapit.R;
import org.astra_g.feedreaderapit.gsons.Item;

import java.util.ArrayList;

public class RssFeedAdapter extends RecyclerView.Adapter<RssFeedAdapter.ViewHolder> {

    private ArrayList<Item> itemList;

    public RssFeedAdapter(ArrayList<Item> itemList) {
        this.itemList = itemList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_row, null, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Item items = itemList.get(position);
        holder.title.setText(items.getTitle());
        holder.creator.setText(items.getCreator());
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.description.setText(Html.fromHtml(items.getDescription(), Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.description.setText(Html.fromHtml(items.getDescription()));
        }
        holder.pubDate.setText(items.getPubDate());
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private TextView creator;
        private TextView description;
        private TextView pubDate;

        public ViewHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.textViewTitle);
            this.creator = (TextView) itemView.findViewById(R.id.textViewCreator);
            this.description = (TextView) itemView.findViewById(R.id.textViewDescription);
            this.pubDate = (TextView) itemView.findViewById(R.id.textViewPubDate);
        }
    }
}