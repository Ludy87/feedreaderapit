package org.astra_g.feedreaderapit.firebase;

import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;

public class FireBase_Instance_ID_Service extends FirebaseInstanceIdService {

    protected final static String TOPIC = "info";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        FirebaseMessaging.getInstance().subscribeToTopic(TOPIC);
    }
}
