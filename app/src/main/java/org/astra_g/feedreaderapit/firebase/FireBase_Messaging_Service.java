package org.astra_g.feedreaderapit.firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.astra_g.feedreaderapit.R;

import java.util.concurrent.atomic.AtomicInteger;

public class FireBase_Messaging_Service extends FirebaseMessagingService {

    private static final String TAG = FireBase_Messaging_Service.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getFrom().equals("/topics/" + FireBase_Instance_ID_Service.TOPIC)) {
            Notification notification = new NotificationCompat.Builder(this)
                    .setContentTitle(String.format("von %s", remoteMessage.getData().get("creator")))
                    .setContentText(remoteMessage.getData().get("title"))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(remoteMessage.getData().get("title")))
                    .build();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(remoteMessage.getData().get("link")));
            PendingIntent contentIntent = PendingIntent.getActivity(getBaseContext(), 0, intent, 0);

            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notification.contentIntent = contentIntent;
            notificationManager.notify(getID(), notification);
        }
    }

    private final static AtomicInteger c = new AtomicInteger(0);

    public static int getID() {
        return c.incrementAndGet();
    }
}
