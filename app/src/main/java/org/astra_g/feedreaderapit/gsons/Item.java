package org.astra_g.feedreaderapit.gsons;

public class Item {

    private String title;
    private String link;
    private String description;
    private String pubDate;
    private String creator;

    public String getCreator() {
        return creator;
    }

    public String getDescription() {
        return description;
    }

    public String getLink() {
        return link;
    }

    public String getPubDate() {
        return pubDate;
    }

    public String getTitle() {
        return title;
    }
}