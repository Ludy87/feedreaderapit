package org.astra_g.feedreaderapit.gsons;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Parent implements Serializable {

    @SerializedName("title")
    private String parentTitle;
    @SerializedName("link")
    private String parentLink;
    @SerializedName("description")
    private String parentDescription;
    @SerializedName("language")
    private String parentLanguage;
    @SerializedName("pubDate")
    private String parentPubDate;
    @SerializedName("creator")
    private String parentCreator;
    @SerializedName("item")
    private Item[] parentItems;

    public String getParentCreator() {
        return (parentCreator);
    }

    public String getParentDescription() {
        return parentDescription;
    }

    public Item[] getParentItems() {
        return parentItems;
    }

    public String getParentLanguage() {
        return parentLanguage;
    }

    public String getParentLink() {
        return parentLink;
    }

    public String getParentPubDate() {
        return parentPubDate;
    }

    public String getParentTitle() {
        return parentTitle;
    }
}